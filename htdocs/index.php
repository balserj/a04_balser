<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>  Assignment 04 -- Balser, Justin </title>
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheetreset" href="css/reset.css">
<link rel="script" href="scripts/process.php">
</head>
<body>

  <section>
    <form method="get" action="scripts/process.php">

<div class="image"><img src="central-park.jpg" alt="park"></div>
<h2 class="titlespacing" style="margin-top:1em; margin-bottom:0">PHOTO DETAILS</h2>
<fieldset style="width:50%; margin-left:25%; border:none">
  <table>
        <tr>Title</tr>
        <br>
        <input style="width:100%" type="text" name="title" placeholder="Give your photo a descriptive name"/>
    <br>
    <br>
    <label>Description</label>
    <br>
    <textarea style="width:100%" type="text" name="description" placeholder="Adding a rich description will help with search results" rows = "6"></textarea>

  <tr style="padding-top:0px">
    <td class="space">
  <label>Continent</label>
  <select style="width:250px" name="where">
    <option>Choose a continent </option>
    <option>Africa</option>
    <option>Antarctica</option>
    <option>Asia</option>
    <option>Australia</option>
    <option>Europe</option>
    <option>North America</option>
    <option>South America</option>
  </select>
  <br>
  <br>
    <label>Country</label>
    <br>
    <select style="width:250px" name="where">
        <option>Choose a country </option>
        <option>Finland</option>
        <option>France</option>
        <option>Poland</option>
        <option>Portugal</option>
        <option>Spain</option>
    </select>
  <br>
  <br>
  <label>City</label>
  <br>
  <input style="width:250px" type="text" name="city" />
  </td>

  <td class="spacing">
    <div style="border:1px solid; padding:.5em">
      <label>Copyright?<br /></label>
        <input type="radio" name="copyright" value="1">All rights reserved<br /></input>
        <input type="radio" name="copyright" value="2" checked>Creative Commons<br /></input>
      </div>
<br>

  <div style="border:1px solid; padding:.5em">
    <label>Creative Commons Types<br /></label>
    <input type="checkbox" name="accept" checked>Attribution<br /></input>
    <input type="checkbox" name="accept">Noncommercial<br /></input>
    <input type="checkbox" name="accept">No Derivative Works<br /></input>
      </div>
    </td>
  </tr>

<br>
<td>
  <br>
  <div class = "backcolor" style="width:275%; padding:1em">
<label>I accept the software license</label>
<input type="checkbox" name="accept" >
</div>
<br>
</td>

<tr>
  <td class="spaces">
  <label>Rate this photo: <br /></label>
    <input type="number" min="1" max="5" name="rate"/>

<br></br>
<label>Color Collection: <br />
<input type="color" name="back" />
</td>

<br>
<td class="padding">
  <div style="border:1px solid darkblue; padding:1em">
<label>Date Taken: <br />
<input type="date"/>

<br></br>

<label>Time Taken: <br />
<input type="time"/>
</div>
</td>
</tr>

  <tr>
  <td colspan="2"; class = "backcolor"; style="width:275%">
    <button type="submit" value="Submit" class="button">Submit</button>
    <button type="reset" value="Reset" class="button">Clear Form</button>
  </td>
</tr>
</table>
<br>


</fieldset>
</form>
</section>


</body>
</html>
